# Brew List

Welcome to the homebrew search list open source project!

This project is designed to search the package manager called homebrew for a
desired string and create a detailed report of the results.

With homebrew one can just type brew search <search string> to get a bare
list of taps and casks that match.  However, to get more information one has
to type a brew command to see details about each line reported.  This python
program will do that for you.  

A set of instructions to install this project is given below.

Please see the issues for this project for possible enhancements.

## Features:

*   Produces two reports:

    *   SearchResultsShortReport.txt - a list of tap names and descriptions as
well as a list of casks identified to match the search string.  (Casks do
not have descriptions at the time this program was written.)

    *   SearchResultsFullReport.txt - an alphabetical list of taps and casks
reported showing their name,  type, description (for taps), home page, 
version, and an already installed indicator.

*   Supports easy python debugging

    To change the debugging level, change the appropriate lines in the 
    [debugging instruction file][debug]

## Requirements

*   Python 3 - a recent version, at least 3.6.

*   The package manager [homebrew][homebrew] (available for MacOS, most
Linuxes and Windows).  See the homebrew [web site][homebrew] for more
information and installation instructions.
 
*   The libraries indicated in requirements.txt.
 
## Code of Conduct

Code of conduct for this project is given in 
[Code of Conduct](Code_of_Conduct.md).

## Licensing

This project is licensed following the MIT licene given in
[License](LICENSE),

## Contributors

Contributers are noted in [Contributors][contrib]

# Quick Start Install

1.  Download the zip file for the master branch of this project.

2.  Unpack the zip file in a convenient directory.

3.  Create a python virtual environment for this project.

    e.g. virtualenv venv
    
4.  Install the various libraries needed.

    pip3 install -r requirements.txt
    
5.  Modify the variables self.search_target and self.false_positives_list in
the BrewSearchListClass to suit.

6.  Run the program (specifying work as the working directory).

7.  Inspect SearchResultsShortReport.txt and SearchResultsFullReport.txt for
the results.

## How to Get Started Contrubuting

Please refer to [Contributors][contrib] for instructions on
how to contribute to this project.

[homebrew]: https://brew.sh/
[debug]: work/debug_info.yaml
[contrib]: Contributors.md