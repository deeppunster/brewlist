"""
BrewList.py - Extract all the items that brew search reports.

This program will
"""

import logging
import logging.config
from dataclasses import dataclass
from enum import Enum
from os import system
import pprint

import yaml  # from PyYAML library
from logging import getLogger, debug, error, info
from pathlib import Path
from typing import Any, Union, Optional

__author__ = 'Travis Risner'
__project__ = "BrewList"
__creation_date__ = "01/03/2020"
# Copyright 2020 by Travis Risner - MIT License

log = None


class BrewType(Enum):
    """
    Indicator of type of brew item (tap or cask)
    """
    tap = 'tap'
    cask = 'cask'


@dataclass
class BrewItem:
    """
    Container of the info for one brew entry.
    """
    name: str
    brew_type: BrewType
    desc: str
    homepage: str
    installed: bool
    version: str


class BrewSearchListClass:
    """
    BrewSearchListClass - Extract all the items that brew search reports.
    """

    def __init__(self):
        self.search_target = 'git'
        self.brew_search_list_name = 'SearchList.txt'
        self.brew_results_full_report_name = 'SearchResultsFullReport.txt'
        self.brew_results_short_report_name = 'SearchResultsShortReport.txt'
        self.tap_list = list()
        self.cask_list = list()
        self.false_positives_list = [
            'snagit',
            'digitemp',
            'sagittarius-scheme',
            'adobe-digital-editions',
            'lego-digital-designer',
            'plotdigitizer',
            'webplotdigitizer',
            'bagit',
            'logitech-presentation'
        ]
        self.false_positives_not_found_list = list()
        self.brew_tap_dict: Optional[dict] = dict()
        self.brew_cask_dict: Optional[dict] = dict()
        self.brew_combined_dict: Optional[dict] = dict()
        # self.pp = pprint.PrettyPrinter(indent=4, depth=5)

    def run_BSL(self):
        """
        Top method for running Run extraction..

        :return:
        """
        info('Creating search list')
        print('Creating search list...')
        self.create_search_list()

        info('Parsing the search list')
        print('Parsing the search list...')
        self.parse_search_list()

        info('Weeding out known items with names not wanted')
        print('Weeding out known items with names not wanted...')
        self.weed_out_false_items()

        info('Building the tap list')
        print('Building the tap list...')
        self.build_tap_dict()

        info('Building the cask list')
        print('Building the cask list...')
        self.build_cask_dict()

        info('Building the short report')
        print('Building the short report...')
        self.build_short_report()

        info('Building the full report')
        print('Building the full report...')
        self.report_results()

        info('Done')
        print('Program finished')
        return

    def create_search_list(self):
        """
        Search all the brew command to find all taps and casks .

        :return:
        """
        brew_cmd = f'brew search {self.search_target} > ' \
                   f'{self.brew_search_list_name}'
        system(brew_cmd)
        return

    def parse_search_list(self):
        """
        Read the search file and pick out the items from it.

        :return:
        """
        type_flag: Optional[BrewType] = None
        with open(self.brew_search_list_name, mode='r') as brewlist:
            for pos, line in enumerate(brewlist, 1):
                line_text = line.strip()
                if line_text == '==> Formulae':
                    type_flag = BrewType.tap
                    continue
                elif line_text == '==> Casks':
                    type_flag = BrewType.cask
                    continue
                elif line_text == '':
                    continue
                else:
                    if type_flag == BrewType.tap:
                        self.tap_list.append(line_text)
                    elif type_flag == BrewType.cask:
                        self.cask_list.append(line_text)
                    else:
                        debug(f'Found {line} at line {pos}')
        debug('\ntaps:')
        debug(pprint.pformat(self.tap_list))
        debug('\ncasks:')
        debug(pprint.pformat(self.cask_list))
        return

    def weed_out_false_items(self):
        """
        Remove false positives from both lists.

        :return:
        """
        # convert to sets, remove duplicates, and restore remaining items.
        tap_set = set(self.tap_list)
        cask_set = set(self.cask_list)
        false_set = set(self.false_positives_list)

        # remove false positives and determine if any false positives not used.
        good_tap_set = tap_set - false_set
        good_cask_set = cask_set - false_set
        bad_false_set = false_set - tap_set - cask_set

        # convert back to lists for use later
        self.tap_list = list(good_tap_set)
        self.cask_list = list(good_cask_set)
        self.false_positives_not_found_list = list(bad_false_set)

        # sort all three lists
        self.tap_list.sort()
        self.cask_list.sort()
        self.false_positives_list.sort()

        # report any false positives not found
        if len(self.false_positives_not_found_list) > 0:
            print(f'These false positives were not found: '
                  f'{self.false_positives_not_found_list}')
            error(f'These false positives not found: '
                  f'{self.false_positives_not_found_list}')
        else:
            print('All false positives found and eliminated')

        debug('\nAfter weeding out false positives\ntaps:')
        debug('\ntaps:')
        debug(pprint.pformat(self.tap_list))
        debug('\ncasks:')
        debug(pprint.pformat(self.cask_list))
        return

    def build_tap_dict(self):
        """
        Get information about each of the tap items and store in the tap dict.

        :return:
        """
        for tap_item in self.tap_list:
            tap_info = self.get_brew_info(tap_item, BrewType.tap)
            self.brew_tap_dict[tap_item] = tap_info
        return

    def build_cask_dict(self):
        """
        Get info about each of the cask items and store in the cask dict.

        :return:
        """
        for cask_item in self.cask_list:
            cask_info = self.get_brew_info(cask_item, BrewType.cask)
            self.brew_cask_dict[cask_item] = cask_info
        return

    def get_brew_info(self, item: str, brew_type: BrewType) -> BrewItem:
        """
        Get brew info for an item, parse it, and return a BrewItem instance.

        :param item: the brew item desired
        :param brew_type: either a tap or a cask
        :return: an instance of BrewItem for the requested item
        """
        # parse out what we want from the text
        results = self.get_brew_text(item, brew_type)
        version = results.__next__()
        if brew_type == BrewType.tap:
            desc = results.__next__()
        else:
            desc = ''
        homepage = results.__next__()
        install_text = results.__next__()
        installed = install_text == "Not installed"
        new_item = BrewItem(
            name=item,
            brew_type=brew_type,
            desc=desc,
            homepage=homepage,
            installed=installed,
            version=version,
        )
        results.close()
        return new_item

    def get_brew_text(self, item: str, brew_type: BrewType):
        # get info text from brew
        if brew_type == BrewType.tap:
            brew_cmd = f'brew info {item}'
        else:
            brew_cmd = f'brew cask info {item}'
        brew_output_file = 'brew_item_text.txt'
        system(f'{brew_cmd} > {brew_output_file}')
        with open(brew_output_file, 'r') as info_text:
            for line in info_text:
                yield line.strip()
        return

    def build_short_report(self):
        """
        Report just the name and description of the search to a file
        :return:
        """
        with open(self.brew_results_short_report_name, mode='w') as report:
            print(f'Short report of all taps for search of'
                  f' {self.search_target}\n', file=report)
            print(f'\nAll taps:\n', file=report)
            for tap in self.brew_tap_dict:
                desc = self.brew_tap_dict[tap].desc
                print(f'     {tap:30} {desc}', file=report)
            print(f'\n\nAll casks:\n', file=report)
            for cask in self.brew_cask_dict:
                print(f'     {cask:30}', file=report)
            print('\nEnd of Report', file=report)
        return

    def report_results(self):
        """
        Report the full results of the search to a file
        :return:
        """
        # create a combined dictionary
        self.brew_combined_dict = self.brew_tap_dict
        self.brew_combined_dict.update(self.brew_cask_dict)
        sorted_items = sorted(self.brew_combined_dict.keys())

        with open(self.brew_results_full_report_name, mode='w') as report:
            print(f'All items reported for search of {self.search_target}\n',
                  file=report)
            for item in sorted_items:
                print(self.report_item(self.brew_combined_dict[item]),
                      file=report)
            print('\nEnd of Report', file=report)
        return

    def report_item(self, item: BrewItem) -> str:
        """
        Format a BrewItem into a nice multiline string.

        :param item: a BrewItem to report
        :return: formatted multiline string
        """
        text = '\n'
        indent = '    '
        text += f'{indent}Name:         {item.name}\n'
        text += f'{indent}Type:         {item.brew_type}\n'
        if len(item.desc) > 0:
            text += f'{indent}Description:  {item.desc}\n'
        text += f'{indent}Homepage:     {item.homepage}\n'
        text += f'{indent}Installed?:   {item.installed}\n'
        text += f'{indent}Verson:       {item.version}\n'
        return text

class Main:
    """
    Main class to start things rolling.
    """

    def __init__(self):
        """
        Get things started.
        """
        self.BrewList = None
        return

    def run_brew_search_list(self):
        """
        Prepare to run Run extraction..

        :return:
        """
        self.BrewList = BrewSearchListClass()
        debug('Starting up BrewList')
        self.BrewList.run_BSL()
        return

    @staticmethod
    def start_logging(work_dir: Path, debug_name: str):
        """
        Establish the logging for all the other scripts.

        :param work_dir:
        :param debug_name:
        :return: (nothing)
        """

        # Set flag that no logging has been established
        logging_started = False

        # find our working directory and possible logging input file
        _workdir = work_dir
        _logfilename = debug_name

        # obtain the full path to the log information
        _debugConfig = _workdir / _logfilename

        # verify that the file exists before trying to open it
        if Path.exists(_debugConfig):
            try:
                #  get the logging params from yaml file and instantiate a log
                with open(_logfilename, 'r') as _logdictfd:
                    _logdict = yaml.load(_logdictfd, Loader=yaml.FullLoader)
                logging.config.dictConfig(_logdict)
                logging_started = True
            except Exception as xcp:
                print(f'The file {_debugConfig} exists, but does not contain '
                      f'appropriate logging directives.')
                raise ValueError('Invalid logging directives.')
        else:
            print(f'Logging directives file {_debugConfig} either not '
                  f'specified or not found')

        if not logging_started:
            # set up minimal logging
            _logfilename = 'debuginfo.txt'
            _debugConfig = _workdir / _logfilename
            logging.basicConfig(filename='debuginfo.txt', level=logging.INFO,
                                filemode='w')
            print(f'Minimal logging established to {_debugConfig}')

        # start logging
        global log
        log = logging.getLogger(__name__)
        logging.info(f'Logging started: working directory is {_workdir}')
        return


if __name__ == "__main__":
    workdir = Path.cwd()
    debug_file_name = 'debug_info.yaml'
    main = Main()
    main.start_logging(workdir, debug_file_name)
    main.run_brew_search_list()

# EOF
